<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Awesome Portfolio</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;1,600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div class="site-main-wrapper">
        <button class="hamburger">
            <img src="{{ asset('img/hamberger.svg') }}" alt="">
        </button>
        <div class="mobile-nav">
            <button class="times"><img src="{{ asset('/img/times.svg') }}" alt=""></button>
            <ul>
                <li><a href="">Home</a></li>
                <li><a href="">About</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">Work</a></li>
                <li><a href="">Blog</a></li>
            </ul>
        </div>
        <header>
            <div class="container">
                <nav id="main-nav" class="flex items-center justify-between">
                    <div class="left flex items-center">
                        <div class="branding">
                            <img src="{{ asset('img/logo-2.png') }}" alt="">
                        </div>
                        <div class="">
                            <a href="">Home</a>
                            <a href="">About</a>
                            <a href="">Services</a>
                            <a href="">Pages</a>
                            <a href="">Blog</a>
                        </div>
                    </div>
                    <div class="right">
                        <button class="btn btn-primary">Contact</button>
                    </div>
                </nav>
                <div class="hero flex items-center justify-between">
                    <div class="left flex-1 flex justify-center">
                        <img src="{{ asset('img/man.png') }}" alt="">
                    </div>
                    <div class="right flex-1">
                        <h6>Praga Hartyanto Prabowo</h6>
                        <h1>I'm a Web <span>Developer</span></h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam minima accusamus eum consequuntur necessitatibus cumque alias numquam blanditiis qui amet..</p>
                        <div>
                            <button  class="btn btn-secondary">Download cv</button>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    
        <section class="about">
            <div class="container flex items-center">
                <div class="flex-1">
                    <img class="about-me-img" src="{{ asset('/img/man-2.png') }}" alt="">
                </div>
                <div class="flex-1">
                    <h1>About <span>Me</span></h1>
                    <h3>Hello i'm Praga Hartyanto Prabowo</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id deleniti dignissimos cupiditate sapiente asperiores quos possimus natus quidem eveniet tenetur labore sequi totam iste repudiandae eum, at soluta repellat quo corrupti nam.</p>
                    <div class="social">
                        <a href=""><img src="{{ asset('/img/website.svg') }}" alt=""></a>
                        <a href=""><img src="{{ asset('/img/facebook.png') }}" alt=""></a>
                        <a href=""><img src="{{ asset('/img/twitter.png') }}" alt=""></a>
                        <a href=""><img src="{{ asset('/img/pinterest.png') }}" alt=""></a>
                        <a href=""><img src="{{ asset('/img/instagram.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="services">
            <div class="container">
                <h1 class="section-heading"><span>Our</span> Services</h1>
                <p>We provide high standar claen website for your business solution</p>
                <div class="card-wrapper">
                    <div class="card">
                        <img src="{{ asset('/img/data-entry.png') }}" alt="">
                        <h2>Data Entry</h2>
                        <p>Kami dapat mencarikan atau memindahkan data dari suatu sumber ke excel, word, website dan lainnya.</p>
                    </div>
                    <div class="card">
                        <img src="{{ asset('/img/code.png') }}" alt="">
                        <h2>Web Programming</h2>
                        <p>Kami dapat membuatkan anda sebuah website dengan menggunakan PHP Laravel.</p>
                    </div>
                    <div class="card">
                        <img src="{{ asset('/img/web-design.png') }}" alt="">
                        <h2>Web Design</h2>
                        <p>Kami dapat membuatkan anda sebuat desain website yang menarik.</p>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="freelancer">
            <h1>I am Available for Freelancer.</h1>
            <p>We provide high standar clean website for your business solutions</p>
            <button class="btn btn-primary">Download CV</button>
        </section>
    
        <section class="reviews">
            <div class="container">
                <h1 class="section-heading"><span>Our</span> Client</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, tempore molestias.</p>
                <div class="slider">
                    <div class="slide">
                        <img src="{{ asset('/img/lady.png') }}" alt="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt nulla fuga animi in nobis totam magni eveniet ducimus dolor blanditiis rem omnis, nemo sapiente repudiandae.</p>
                        <span> - Guest, Company A</span>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('/img/lady.png') }}" alt="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt nulla fuga animi in nobis totam magni eveniet ducimus dolor blanditiis rem omnis, nemo sapiente repudiandae.</p>
                        <span> - Guest, Company A</span>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('/img/lady.png') }}" alt="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt nulla fuga animi in nobis totam magni eveniet ducimus dolor blanditiis rem omnis, nemo sapiente repudiandae.</p>
                        <span> - Guest, Company A</span>
                    </div>
                </div>
                <div class="slider-dots"></div>
            </div>
        </section>
    
        <section class="work">
            <div class="container">
                <h1 class="section-heading"><span>Our</span> Works</h1>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione perspiciatis molestiae fugiat.</p>
                <div class="card-wrapper">
                    <div class="card">
                        <img src="{{ asset('/img/ph-1.png') }}" alt="">
                    </div>
                    <div class="card">
                        <img src="{{ asset('/img/ph-1.png') }}" alt="">
                    </div>
                    <div class="card">
                        <img src="{{ asset('/img/ph-1.png') }}" alt="">
                    </div>
                </div>
            </div>
        </section>
    
        <section class="blog">
            <div class="container">
                <h1 class="section-heading"><span>Our </span>Blog</h1>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Velit, id.</p>
    
                <div class="card-wrapper">
                    <div class="card">
                        <div class="img-wrapper">
                            <img src="{{ asset('img/article-ph-1.png') }}" alt="" srcset="">
                        </div>
                        <div class="card-content">
                            <a href="">
                                <h1>Lorem ipsum dolor sit amet.</h1>
                            </a>
                            <span>May 12, 2020</span>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur possimus error cum, quae vitae ducimus at suscipit, cumque sunt non eum odit fugit? Placeat, repudiandae.</p>
                            <a href="" class="read-more">Read more</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="img-wrapper">
                            <img src="{{ asset('img/article-ph-1.png') }}" alt="" srcset="">
                        </div>
                        <div class="card-content">
                            <a href="">
                                <h1>Lorem ipsum dolor sit amet.</h1>
                            </a>
                            <span>May 12, 2020</span>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur possimus error cum, quae vitae ducimus at suscipit, cumque sunt non eum odit fugit? Placeat, repudiandae.</p>
                            <a href="" class="read-more">Read more</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="img-wrapper">
                            <img src="{{ asset('img/article-ph-1.png') }}" alt="" srcset="">
                        </div>
                        <div class="card-content">
                            <a href="">
                                <h1>Lorem ipsum dolor sit amet.</h1>
                            </a>
                            <span>May 12, 2020</span>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur possimus error cum, quae vitae ducimus at suscipit, cumque sunt non eum odit fugit? Placeat, repudiandae.</p>
                            <a href="" class="read-more">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="contact">
            <div class="container">
                <h1 class="section-heading"><span>Our </span>Contact</h1>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit.</p>
                <div class="card-wrapper">
                    <div class="card">
                        <img src="{{ asset('/img/phone-2.svg') }}" alt="">
                        <h2>Calls us on</h2>
                        <h6>+62822 2173 7280</h6>
                    </div>
                    <div class="card">
                        <img src="{{ asset('/img/phone-2.svg') }}" alt="">
                        <h2>Email us at</h2>
                        <h6>hartyanto@gmail.com</h6>
                    </div>
                    <div class="card">
                        <img src="{{ asset('/img/phone-2.svg') }}" alt="">
                        <h2>Visit ofiice</h2>
                        <h6>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus, possimus?</h6>
                    </div>
                </div>
    
                <form action="" method="post">
                    <div class="input-wrap">
                        <input type="text" placeholder="Your Name" name="" id="">
                        <input type="email" placeholder="Your Email" name="" id="">
                    </div>
                    <div class="input-wrap-2">
                        <input type="text" placeholder="Your Subject..." name="" id="">
                        <textarea name="" placeholder="Your Message" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div class="flex justify-center">
                        <button class="btn btn-primary">Send Message</button>
                    </div>
                </form>
    
            </div>
        </section>
    
        <footer>
            <img class="footer-logo" src="{{  asset('img/logo-2.png') }}" alt="" srcset="">
            <div class="footer-socials">
                <a href=""><img src=" {{ asset('img/website.png') }}" alt=""></a>
                <a href=""><img src=" {{ asset('img/instagram.png') }}" alt=""></a>
                <a href=""><img src=" {{ asset('img/twitter.png') }}" alt=""></a>
                <a href=""><img src=" {{ asset('img/facebook.png') }}" alt=""></a>
                <a href=""><img src=" {{ asset('img/pinterest.png') }}" alt=""></a>
            </div>
            <div class="copyright">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Numquam, molestias?
            </div>
        </footer>
    </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>